ライセンス
==============

本プロジェクトは
`Apache License, Version 2.0 <http://www.apache.org/licenses/LICENSE-2.0>`_
です。
ただし、cmake ディレクトリの中にある一部のファイルは個別のライセンスがあり、
そのライセンスの表示が冒頭にあります。
