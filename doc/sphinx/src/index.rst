fluid-simulation
============================

数値流体力学の実験を行う。

リポジトリ：
`https://gitlab.com/MusicScience37Projects/numerical-analysis/fluid-simulation <https://gitlab.com/MusicScience37Projects/numerical-analysis/fluid-simulation>`_

API リファレンスは `ここから <api/index.html>`_。

.. toctree::
    :maxdepth: 1

    theory/index
    reference
    license
