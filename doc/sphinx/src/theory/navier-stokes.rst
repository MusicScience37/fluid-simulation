Navier-Stokes 方程式
======================

記号
---------

- :math:`\rho`: 密度
- :math:`\bm{u} = (u_1, u_2, u_3)`: 流れの速度
- :math:`p`: 圧力（基準値からの差とすることが多い）
- :math:`\bm{S} = (S_1, S_2, S_3)`: 生成項（追加で力が働く場合に用いられる）
- :math:`\mu`: 粘性係数

連続方程式
-------------

連続方程式は一般に次のように書ける :cite:p:`Arakawa2008` :cite:p:`Okamoto2017`。

.. math::

    \frac{\partial}{\partial t} \rho + \sum_j \frac{\partial}{\partial x_j} (\rho u_j) = 0

特に、非圧縮流れ（密度の変化がない場合）は次のようになる :cite:p:`Arakawa2008` :cite:p:`Okamoto2017`。

.. math::

    \sum_j \frac{\partial u_j}{\partial x_j} = 0

運動方程式
--------------

運動方程式は次のように書ける :cite:p:`Arakawa2008` :cite:p:`Okamoto2017`。

.. math::

    \frac{\partial}{\partial t}(\rho u_i) + \sum_j \frac{\partial}{\partial x_j} (\rho u_i u_j)
    = \sum_j \frac{\partial}{\partial x_j} \left(\mu \frac{\partial u_i}{\partial x_j} \right)
    - \frac{\partial p}{\partial x_i} + S_i
