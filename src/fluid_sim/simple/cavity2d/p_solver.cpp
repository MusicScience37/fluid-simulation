/*
 * Copyright 2021 MusicScience37 (Kenta Kabashima)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*!
 * \file
 * \brief Implementation of p_solver class.
 */
#include "fluid_sim/simple/cavity2d/p_solver.h"

#include <num_collect/base/index_type.h>

#include "fluid_sim/simple/cavity2d/u_solver.h"
#include "fluid_sim/simple/cavity2d/v_solver.h"

namespace fluid_sim::simple::cavity2d {

void p_solver::init(const simple_cavity2d_config& config, const grid& grid,
    const u_solver& us, const v_solver& vs, const fluid_variables& variables) {
    const num_collect::index_type x_size = variables.p.rows();
    const num_collect::index_type y_size = variables.p.cols();

    equation_.ap = matrix_type::Zero(x_size, y_size);
    equation_.an = matrix_type::Zero(x_size, y_size);
    equation_.as = matrix_type::Zero(x_size, y_size);
    equation_.aw = matrix_type::Zero(x_size, y_size);
    equation_.ae = matrix_type::Zero(x_size, y_size);
    equation_.b = matrix_type::Zero(x_size, y_size);

    p_update_ = matrix_type::Zero(x_size, y_size);

    xu_dist_ = grid.xu.segment(2, x_size - 2) - grid.xu.segment(1, x_size - 2);
    yv_dist_ = grid.yv.segment(2, y_size - 2) - grid.yv.segment(1, y_size - 2);

    update_equation(config, us, vs, variables);

    solver_.init(1, x_size - 2, 1, y_size - 2, p_update_);
    residual_norm_before_update_ = solver_.residual_norm(equation_, p_update_);
}

void p_solver::solve(const simple_cavity2d_config& config, const grid& grid,
    const u_solver& us, const v_solver& vs, fluid_variables& variables) {
    (void)grid;

    update_equation(config, us, vs, variables);

    residual_norm_before_update_ = solver_.residual_norm(equation_, p_update_);

    update_variables(config, us, vs, variables);

    residual_norm_ = solver_.residual_norm(equation_, p_update_);
}

auto p_solver::equation() const -> const linear_equation& { return equation_; }

auto p_solver::residual_norm() const -> double { return residual_norm_; }

auto p_solver::residual_norm_before_update() const -> double {
    return residual_norm_before_update_;
}

void p_solver::update_equation(const simple_cavity2d_config& config,
    const u_solver& us, const v_solver& vs, const fluid_variables& variables) {
    const num_collect::index_type x_size = variables.u.rows();
    const num_collect::index_type y_size = variables.u.cols();

    equation_.an.middleRows(1, x_size - 2).middleCols(1, y_size - 2) =
        config.density *
        (vs.pressure_rate()
                .middleRows(1, x_size - 2)
                .middleCols(2, y_size - 2)
                .array()
                .colwise() *
            xu_dist_.array());
    equation_.as.middleRows(1, x_size - 2).middleCols(1, y_size - 2) =
        config.density *
        (vs.pressure_rate()
                .middleRows(1, x_size - 2)
                .middleCols(1, y_size - 2)
                .array()
                .colwise() *
            xu_dist_.array());
    equation_.ae.middleRows(1, x_size - 2).middleCols(1, y_size - 2) =
        config.density *
        (us.pressure_rate()
                .middleRows(2, x_size - 2)
                .middleCols(1, y_size - 2)
                .array()
                .rowwise() *
            yv_dist_.transpose().array());
    equation_.aw.middleRows(1, x_size - 2).middleCols(1, y_size - 2) =
        config.density *
        (us.pressure_rate()
                .middleRows(1, x_size - 2)
                .middleCols(1, y_size - 2)
                .array()
                .rowwise() *
            yv_dist_.transpose().array());
    equation_.ap.middleRows(1, x_size - 2).middleCols(1, y_size - 2) =
        equation_.an.middleRows(1, x_size - 2).middleCols(1, y_size - 2) +
        equation_.as.middleRows(1, x_size - 2).middleCols(1, y_size - 2) +
        equation_.ae.middleRows(1, x_size - 2).middleCols(1, y_size - 2) +
        equation_.aw.middleRows(1, x_size - 2).middleCols(1, y_size - 2);
    equation_.b.middleRows(1, x_size - 2).middleCols(1, y_size - 2) =
        config.density *
            ((variables.u.middleRows(1, x_size - 2).middleCols(1, y_size - 2) -
                 variables.u.middleRows(2, x_size - 2)
                     .middleCols(1, y_size - 2))
                    .array()
                    .rowwise() *
                yv_dist_.transpose().array()) +
        config.density *
            ((variables.v.middleRows(1, x_size - 2).middleCols(1, y_size - 2) -
                 variables.v.middleRows(1, x_size - 2)
                     .middleCols(2, y_size - 2))
                    .array()
                    .colwise() *
                xu_dist_.array());

    p_update_.setZero();
}

void p_solver::update_variables(const simple_cavity2d_config& config,
    const u_solver& us, const v_solver& vs, fluid_variables& variables) {
    const num_collect::index_type x_size = variables.u.rows();
    const num_collect::index_type y_size = variables.u.cols();

    for (num_collect::index_type i = 0; i < config.p_tdma_iterations; ++i) {
        solver_.solve_for_y(equation_, p_update_);
    }

    variables.p += config.p_relax_coeff * p_update_;
    variables.u.middleRows(2, x_size - 3).middleCols(1, y_size - 2) +=
        config.p_relax_coeff *
        us.pressure_rate()
            .middleRows(2, x_size - 3)
            .middleCols(1, y_size - 2)
            .cwiseProduct(
                p_update_.middleRows(1, x_size - 3).middleCols(1, y_size - 2) -
                p_update_.middleRows(2, x_size - 3).middleCols(1, y_size - 2));
    variables.v.middleRows(1, x_size - 2).middleCols(2, y_size - 3) +=
        config.p_relax_coeff *
        vs.pressure_rate()
            .middleRows(1, x_size - 2)
            .middleCols(2, y_size - 3)
            .cwiseProduct(
                p_update_.middleRows(1, x_size - 2).middleCols(1, y_size - 3) -
                p_update_.middleRows(1, x_size - 2).middleCols(2, y_size - 3));
}

}  // namespace fluid_sim::simple::cavity2d
