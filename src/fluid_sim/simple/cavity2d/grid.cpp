/*
 * Copyright 2021 MusicScience37 (Kenta Kabashima)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*!
 * \file
 * \brief Implementation of grid class.
 */
#include "fluid_sim/simple/cavity2d/grid.h"

#include <num_collect/base/index_type.h>

namespace fluid_sim::simple::cavity2d {

auto make_grid(const simple_cavity2d_config& config) -> grid {
    grid g{};

    const double width = config.width;
    const double height = config.height;
    const num_collect::index_type x_points = config.x_points;
    const num_collect::index_type y_points = config.y_points;

    // Number of points including points out of the cavity used in calculation.
    const num_collect::index_type whole_x_points = x_points + 2;
    const num_collect::index_type whole_y_points = y_points + 2;

    g.x.resize(whole_x_points);
    for (num_collect::index_type i = 0; i < whole_x_points; ++i) {
        const double rate =
            (static_cast<double>(i) - 0.5) / static_cast<double>(x_points);
        g.x(i) = rate * width;
    }
    g.xu.resize(whole_x_points);
    for (num_collect::index_type i = 0; i < whole_x_points; ++i) {
        const double rate =
            (static_cast<double>(i) - 1.0) / static_cast<double>(x_points);
        g.xu(i) = rate * width;
    }

    g.y.resize(whole_y_points);
    for (num_collect::index_type i = 0; i < whole_y_points; ++i) {
        const double rate =
            (static_cast<double>(i) - 0.5) / static_cast<double>(y_points);
        g.y(i) = rate * height;
    }
    g.yv.resize(whole_y_points);
    for (num_collect::index_type i = 0; i < whole_y_points; ++i) {
        const double rate =
            (static_cast<double>(i) - 1.0) / static_cast<double>(y_points);
        g.yv(i) = rate * height;
    }

    return g;
}

}  // namespace fluid_sim::simple::cavity2d
