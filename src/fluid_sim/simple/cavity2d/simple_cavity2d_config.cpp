/*
 * Copyright 2021 MusicScience37 (Kenta Kabashima)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*!
 * \file
 * \brief Implementation of simple_cavity2d_config class.
 */
#include "fluid_sim/simple/cavity2d/simple_cavity2d_config.h"

#include <string>
#include <string_view>

#include <num_collect/logging/logger.h>
#include <toml++/toml.h>

namespace fluid_sim::simple::cavity2d {

/*!
 * \brief Load a value if exists.
 *
 * \tparam T Type of the value.
 * \param[in,out] target Value. (Existing value is treated as the default.)
 * \param[in] table Table.
 * \param[in] name Name of the value.
 */
template <typename T>
static void load_if_exists(
    T& target, const toml::table* table, std::string_view name) {
    const auto* node = table->get(name);
    if (node == nullptr) {
        num_collect::logging::logger().trace()(
            "Configuration {}: {} (default)", name, target);
        return;
    }
    const auto value = node->value<T>();
    if (!value) {
        num_collect::logging::logger().trace()(
            "Configuration {}: {} (default)", name, target);
        return;
    }
    target = value.value();
    num_collect::logging::logger().info()(
        "Configuration {}: {} (specified)", name, target);
}

auto load_simple_cavity2d_config(const std::string& filepath)
    -> simple_cavity2d_config {
    simple_cavity2d_config config;

    num_collect::logging::logger().info()("Loading {}", filepath);
    const auto root = toml::parse_file(filepath);
    const auto* table = root.as_table()->at("simple_cavity2d").as_table();

    load_if_exists(config.density, table, "density");
    load_if_exists(config.viscosity, table, "viscosity");
    load_if_exists(config.width, table, "width");
    load_if_exists(config.height, table, "height");
    load_if_exists(config.upper_wall_velocity, table, "upper_wall_velocity");
    load_if_exists(config.x_points, table, "x_points");
    load_if_exists(config.y_points, table, "y_points");
    load_if_exists(config.u_tdma_iterations, table, "u_tdma_iterations");
    load_if_exists(config.v_tdma_iterations, table, "v_tdma_iterations");
    load_if_exists(config.p_tdma_iterations, table, "p_tdma_iterations");
    load_if_exists(config.u_relax_coeff, table, "u_relax_coeff");
    load_if_exists(config.v_relax_coeff, table, "v_relax_coeff");
    load_if_exists(config.p_relax_coeff, table, "p_relax_coeff");
    load_if_exists(config.max_iterations, table, "max_iterations");
    load_if_exists(
        config.residual_tolerance_rate, table, "residual_tolerance_rate");
    load_if_exists(config.binary_output, table, "binary_output");
    load_if_exists(config.output_filepath, table, "output_filepath");

    return config;
}

}  // namespace fluid_sim::simple::cavity2d
