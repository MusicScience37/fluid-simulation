/*
 * Copyright 2021 MusicScience37 (Kenta Kabashima)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*!
 * \file
 * \brief Implementation of vtk_file_writer class.
 */
#include "fluid_sim/simple/cavity2d/vtk_file_writer.h"

#include <memory>

#include <num_collect/base/index_type.h>
#include <num_collect/logging/logging_mixin.h>
#include <vtkCellData.h>
#include <vtkDoubleArray.h>
#include <vtkNew.h>
#include <vtkPoints.h>
#include <vtkStructuredGrid.h>
#include <vtkXMLStructuredGridWriter.h>

namespace fluid_sim::simple::cavity2d {

vtk_file_writer::vtk_file_writer()
    : num_collect::logging::logging_mixin(vtk_file_writer_tag) {}

void vtk_file_writer::write(const simple_cavity2d_config& config,
    const grid& grid, const fluid_variables& variables,
    const std::string& filepath) {
    // VTK uses int .
    const int x_ind_min = 1;
    const int x_ind_max = static_cast<int>(config.x_points + 1);
    const int x_size = x_ind_max - x_ind_min + 1;

    const int y_ind_min = 1;
    const int y_ind_max = static_cast<int>(config.y_points + 1);
    const int y_size = y_ind_max - y_ind_min + 1;

    const int num_cells = (x_size - 1) * (y_size - 1);

    vtkNew<vtkPoints> vtk_points;
    vtkNew<vtkDoubleArray> vtk_u;
    vtk_u->SetName("Velocity");
    vtk_u->SetNumberOfComponents(3);
    vtk_u->SetNumberOfTuples(num_cells);
    vtkNew<vtkDoubleArray> vtk_p;
    vtk_p->SetName("Pressure");
    vtk_p->SetNumberOfComponents(1);
    vtk_p->SetNumberOfTuples(num_cells);
    int ind = 0;
    for (int k = 0; k <= 1; ++k) {
        const double z = static_cast<double>(k) * 0.1 * config.width;
        for (int j = y_ind_min; j <= y_ind_max; ++j) {
            for (int i = x_ind_min; i <= x_ind_max; ++i) {
                vtk_points->InsertNextPoint(grid.xu(i), grid.yv(j), z);

                if (i < x_ind_max && j < y_ind_max && k == 0) {
                    const double est_u =
                        0.5 * (variables.u(i, j) + variables.u(i + 1, j));
                    const double est_v =
                        0.5 * (variables.v(i, j) + variables.v(i, j + 1));
                    vtk_u->SetTuple3(ind, est_u, est_v, 0.0);

                    vtk_p->SetTuple1(ind, variables.p(i, j));

                    ++ind;
                }
            }
        }
    }

    vtkNew<vtkStructuredGrid> vtk_grid;
    vtk_grid->SetDimensions(x_size, y_size, 2);
    vtk_grid->SetPoints(vtk_points.Get());
    vtk_grid->GetCellData()->AddArray(vtk_u.Get());
    vtk_grid->GetCellData()->AddArray(vtk_p.Get());

    vtkNew<vtkXMLStructuredGridWriter> vtk_writer;
    vtk_writer->SetFileName(filepath.c_str());
    vtk_writer->SetInputData(vtk_grid.Get());
    if (config.binary_output) {
        vtk_writer->SetDataModeToBinary();
        vtk_writer->SetCompressorTypeToZLib();
    } else {
        vtk_writer->SetDataModeToAscii();
    }
    vtk_writer->Write();

    this->logger().info()("Wrote {}.", filepath);
}

}  // namespace fluid_sim::simple::cavity2d
