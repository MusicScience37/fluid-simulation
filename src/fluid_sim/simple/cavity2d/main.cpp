/*
 * Copyright 2021 MusicScience37 (Kenta Kabashima)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*!
 * \file
 * \brief Definition of main function.
 */
#include <cmath>
#include <exception>
#include <iostream>
#include <string>
#include <string_view>

#include <num_collect/base/index_type.h>
#include <num_collect/logging/iterations/iteration_logger.h>
#include <num_collect/logging/load_logging_config.h>
#include <num_collect/logging/log_config.h>
#include <num_collect/logging/log_level.h>
#include <num_collect/logging/log_tag_config.h>
#include <num_collect/logging/log_tag_view.h>
#include <num_collect/logging/logger.h>

#include "fluid_sim/simple/cavity2d/fluid_variables.h"
#include "fluid_sim/simple/cavity2d/grid.h"
#include "fluid_sim/simple/cavity2d/p_solver.h"
#include "fluid_sim/simple/cavity2d/simple_cavity2d_config.h"
#include "fluid_sim/simple/cavity2d/u_solver.h"
#include "fluid_sim/simple/cavity2d/v_solver.h"
#include "fluid_sim/simple/cavity2d/vtk_file_writer.h"

#ifndef FLUID_SIM_DOCUMENTATION

static void configure_logging(std::string_view config_filepath) {
    if (config_filepath.empty()) {
        num_collect::logging::log_config::instance().set_default_tag_config(
            num_collect::logging::log_tag_config()
                .output_log_level(num_collect::logging::log_level::iteration)
                .iteration_output_period(10));  // NOLINT
    } else {
        num_collect::logging::load_logging_config_file(config_filepath);
    }
}

static void solve(std::string_view config_filepath) {
    fluid_sim::simple::cavity2d::simple_cavity2d_config config;
    if (!config_filepath.empty()) {
        config = fluid_sim::simple::cavity2d::load_simple_cavity2d_config(
            std::string(config_filepath));
    }
    const auto grid = fluid_sim::simple::cavity2d::make_grid(config);
    auto variables = fluid_sim::simple::cavity2d::make_fluid_variables(grid);

    fluid_sim::simple::cavity2d::u_solver u_solver;
    fluid_sim::simple::cavity2d::v_solver v_solver;
    fluid_sim::simple::cavity2d::p_solver p_solver;

    u_solver.init(config, grid, variables);
    v_solver.init(config, grid, variables);
    p_solver.init(config, grid, u_solver, v_solver, variables);

    num_collect::index_type iterations = 0;

    num_collect::logging::logger logger{
        num_collect::logging::log_tag_view("simple_cavity2d")};
    num_collect::logging::iterations::iteration_logger iter_logger{logger};
    iter_logger.append("Iter.", iterations);
    iter_logger.append<double>("Res.U",
        [&u_solver] { return u_solver.residual_norm_before_update(); });
    iter_logger.append<double>(
        "Res.U(a)", [&u_solver] { return u_solver.residual_norm(); });
    iter_logger.append<double>("Res.V",
        [&v_solver] { return v_solver.residual_norm_before_update(); });
    iter_logger.append<double>(
        "Res.V(a)", [&v_solver] { return v_solver.residual_norm(); });
    iter_logger.append<double>("Res.P",
        [&p_solver] { return p_solver.residual_norm_before_update(); });
    iter_logger.append<double>(
        "Res.P(a)", [&p_solver] { return p_solver.residual_norm(); });

    // Calculate tolerances.
    const double u_residual_tolerance = config.residual_tolerance_rate *
        config.width / static_cast<double>(config.x_points) * config.density *
        config.upper_wall_velocity * config.upper_wall_velocity *
        std::sqrt(static_cast<double>(config.x_points * config.y_points));
    const double v_residual_tolerance = u_residual_tolerance;
    const double p_residual_tolerance = config.residual_tolerance_rate *
        config.height / static_cast<double>(config.y_points) * config.density *
        config.upper_wall_velocity *
        std::sqrt(static_cast<double>(config.x_points * config.y_points));
    logger.info()("Tolerance of residuals. U: {:.3e}, V: {:.3e}, P: {:.3e}.",
        u_residual_tolerance, v_residual_tolerance, p_residual_tolerance);

    iter_logger.write_iteration();
    for (iterations = 1; iterations <= config.max_iterations; ++iterations) {
        u_solver.solve(config, grid, variables);
        v_solver.solve(config, grid, variables);
        p_solver.solve(config, grid, u_solver, v_solver, variables);
        iter_logger.write_iteration();
        if (u_solver.residual_norm_before_update() < u_residual_tolerance &&
            v_solver.residual_norm_before_update() < v_residual_tolerance &&
            p_solver.residual_norm_before_update() < p_residual_tolerance) {
            break;
        }
    }
    iter_logger.write_summary();

    fluid_sim::simple::cavity2d::vtk_file_writer writer;
    writer.write(config, grid, variables, "simple_cavity2d.vts");
}

auto main(int argc, char** argv) -> int {
    try {
        std::string_view config_filepath;
        if (argc == 2) {
            config_filepath = argv[1];  // NOLINT
        }
        configure_logging(config_filepath);
        solve(config_filepath);
    } catch (const std::exception& e) {
        std::cerr << e.what() << std::endl;
    }

    return 0;
}

#endif
