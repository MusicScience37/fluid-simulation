/*
 * Copyright 2021 MusicScience37 (Kenta Kabashima)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*!
 * \file
 * \brief Implementation of tdma_linear_solver class.
 */
#include "fluid_sim/simple/cavity2d/tdma_linear_solver.h"

#include <num_collect/base/exception.h>
#include <num_collect/base/index_type.h>
#include <num_collect/logging/log_and_throw.h>
#include <num_collect/util/assert.h>

namespace fluid_sim::simple::cavity2d {

void tdma_linear_solver::init(num_collect::index_type x_min_ind,
    num_collect::index_type x_max_ind, num_collect::index_type y_min_ind,
    num_collect::index_type y_max_ind, const matrix_type& mat) {
    NUM_COLLECT_ASSERT(x_min_ind >= 1);
    NUM_COLLECT_ASSERT(x_min_ind < x_max_ind <= mat.rows() - 2);
    NUM_COLLECT_ASSERT(y_min_ind >= 1);
    NUM_COLLECT_ASSERT(y_min_ind < y_max_ind <= mat.cols() - 2);

    x_min_ind_ = x_min_ind;
    x_max_ind_ = x_max_ind;
    y_min_ind_ = y_min_ind;
    y_max_ind_ = y_max_ind;

    p_.resizeLike(mat);
    q_.resizeLike(mat);
}

void tdma_linear_solver::solve_for_y(
    const linear_equation& equation, matrix_type& x) {
    const num_collect::index_type x_size = x_max_ind_ - x_min_ind_ + 1;

    const auto& ap = equation.ap;
    const auto& an = equation.an;
    const auto& as = equation.as;
    const auto& aw = equation.aw;
    const auto& ae = equation.ae;
    const auto& b = equation.b;

    {
        const num_collect::index_type i = y_min_ind_;
        den_ = ap.col(i).segment(x_min_ind_, x_size).cwiseInverse();
        if (den_.array().isNaN().any()) {
            num_collect::logging::log_and_throw<num_collect::algorithm_failure>(
                "Failed to solve equation.");
        }

        p_.col(i).segment(x_min_ind_, x_size) =
            an.col(i).segment(x_min_ind_, x_size).cwiseProduct(den_);
        q_.col(i).segment(x_min_ind_, x_size) =
            (as.col(i)
                    .segment(x_min_ind_, x_size)
                    .cwiseProduct(x.col(i - 1).segment(x_min_ind_, x_size)) +
                aw.col(i)
                    .segment(x_min_ind_, x_size)
                    .cwiseProduct(x.col(i).segment(x_min_ind_ - 1, x_size)) +
                ae.col(i)
                    .segment(x_min_ind_, x_size)
                    .cwiseProduct(x.col(i).segment(x_min_ind_ + 1, x_size)) +
                b.col(i).segment(x_min_ind_, x_size))
                .cwiseProduct(den_);
    }
    for (num_collect::index_type i = y_min_ind_ + 1; i < y_max_ind_; ++i) {
        den_ = (ap.col(i).segment(x_min_ind_, x_size) -
            as.col(i)
                .segment(x_min_ind_, x_size)
                .cwiseProduct(p_.col(i - 1).segment(x_min_ind_, x_size)))
                   .cwiseInverse();
        if (den_.array().isNaN().any()) {
            num_collect::logging::log_and_throw<num_collect::algorithm_failure>(
                "Failed to solve equation.");
        }

        p_.col(i).segment(x_min_ind_, x_size) =
            an.col(i).segment(x_min_ind_, x_size).cwiseProduct(den_);
        q_.col(i).segment(x_min_ind_, x_size) =
            (as.col(i)
                    .segment(x_min_ind_, x_size)
                    .cwiseProduct(q_.col(i - 1).segment(x_min_ind_, x_size)) +
                aw.col(i)
                    .segment(x_min_ind_, x_size)
                    .cwiseProduct(x.col(i).segment(x_min_ind_ - 1, x_size)) +
                ae.col(i)
                    .segment(x_min_ind_, x_size)
                    .cwiseProduct(x.col(i).segment(x_min_ind_ + 1, x_size)) +
                b.col(i).segment(x_min_ind_, x_size))
                .cwiseProduct(den_);
    }
    {
        const num_collect::index_type i = y_max_ind_;
        den_ = (ap.col(i).segment(x_min_ind_, x_size) -
            as.col(i)
                .segment(x_min_ind_, x_size)
                .cwiseProduct(p_.col(i - 1).segment(x_min_ind_, x_size)))
                   .cwiseInverse();
        if (den_.array().isNaN().any()) {
            num_collect::logging::log_and_throw<num_collect::algorithm_failure>(
                "Failed to solve equation.");
        }

        x.col(i).segment(x_min_ind_, x_size) =
            (as.col(i)
                    .segment(x_min_ind_, x_size)
                    .cwiseProduct(q_.col(i - 1).segment(x_min_ind_, x_size)) +
                an.col(i)
                    .segment(x_min_ind_, x_size)
                    .cwiseProduct(den_)
                    .cwiseProduct(x.col(i + 1).segment(x_min_ind_, x_size)) +
                aw.col(i)
                    .segment(x_min_ind_, x_size)
                    .cwiseProduct(x.col(i).segment(x_min_ind_ - 1, x_size)) +
                ae.col(i)
                    .segment(x_min_ind_, x_size)
                    .cwiseProduct(x.col(i).segment(x_min_ind_ + 1, x_size)) +
                b.col(i).segment(x_min_ind_, x_size))
                .cwiseProduct(den_);
    }
    for (num_collect::index_type i = y_max_ind_ - 1; i >= y_min_ind_; --i) {
        x.col(i).segment(x_min_ind_, x_size) =
            p_.col(i)
                .segment(x_min_ind_, x_size)
                .cwiseProduct(x.col(i + 1).segment(x_min_ind_, x_size)) +
            q_.col(i).segment(x_min_ind_, x_size);
    }
}

auto tdma_linear_solver::residual_norm(
    const linear_equation& equation, const matrix_type& x) const -> double {
    const num_collect::index_type x_size = x_max_ind_ - x_min_ind_ + 1;
    const num_collect::index_type y_size = y_max_ind_ - y_min_ind_ + 1;

    const auto& ap = equation.ap;
    const auto& an = equation.an;
    const auto& as = equation.as;
    const auto& aw = equation.aw;
    const auto& ae = equation.ae;
    const auto& b = equation.b;

    return (ap.middleRows(x_min_ind_, x_size)
                .middleCols(y_min_ind_, y_size)
                .cwiseProduct(x.middleRows(x_min_ind_, x_size)
                                  .middleCols(y_min_ind_, y_size)) -
        an.middleRows(x_min_ind_, x_size)
            .middleCols(y_min_ind_, y_size)
            .cwiseProduct(x.middleRows(x_min_ind_, x_size)
                              .middleCols(y_min_ind_ + 1, y_size)) -
        as.middleRows(x_min_ind_, x_size)
            .middleCols(y_min_ind_, y_size)
            .cwiseProduct(x.middleRows(x_min_ind_, x_size)
                              .middleCols(y_min_ind_ - 1, y_size)) -
        aw.middleRows(x_min_ind_, x_size)
            .middleCols(y_min_ind_, y_size)
            .cwiseProduct(x.middleRows(x_min_ind_ - 1, x_size)
                              .middleCols(y_min_ind_, y_size)) -
        ae.middleRows(x_min_ind_, x_size)
            .middleCols(y_min_ind_, y_size)
            .cwiseProduct(x.middleRows(x_min_ind_ + 1, x_size)
                              .middleCols(y_min_ind_, y_size)) -
        b.middleRows(x_min_ind_, x_size).middleCols(y_min_ind_, y_size))
        .norm();
}

}  // namespace fluid_sim::simple::cavity2d
