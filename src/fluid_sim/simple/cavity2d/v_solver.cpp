/*
 * Copyright 2021 MusicScience37 (Kenta Kabashima)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*!
 * \file
 * \brief Implementation of v_solver class.
 */
#include "fluid_sim/simple/cavity2d/v_solver.h"

#include <num_collect/base/index_type.h>

#include "fluid_sim/simple/cavity2d/types.h"

namespace fluid_sim::simple::cavity2d {

void v_solver::init(const simple_cavity2d_config& config, const grid& grid,
    const fluid_variables& variables) {
    const num_collect::index_type x_size = variables.v.rows();
    const num_collect::index_type y_size = variables.v.cols();

    equation_.ap = matrix_type::Zero(x_size, y_size);
    equation_.an = matrix_type::Zero(x_size, y_size);
    equation_.as = matrix_type::Zero(x_size, y_size);
    equation_.aw = matrix_type::Zero(x_size, y_size);
    equation_.ae = matrix_type::Zero(x_size, y_size);
    equation_.b = matrix_type::Zero(x_size, y_size);

    pressure_rate_ = matrix_type::Zero(x_size, y_size);

    cn_ = matrix_type::Zero(x_size - 2, y_size - 3);
    dn_ = matrix_type::Zero(x_size - 2, y_size - 3);
    cs_ = matrix_type::Zero(x_size - 2, y_size - 3);
    ds_ = matrix_type::Zero(x_size - 2, y_size - 3);
    ce_ = matrix_type::Zero(x_size - 2, y_size - 3);
    de_ = matrix_type::Zero(x_size - 2, y_size - 3);
    cw_ = matrix_type::Zero(x_size - 2, y_size - 3);
    dw_ = matrix_type::Zero(x_size - 2, y_size - 3);

    y_dist_ = grid.y.segment(2, y_size - 3) - grid.y.segment(1, y_size - 3);
    yv_dist_ = grid.yv.segment(2, y_size - 2) - grid.yv.segment(1, y_size - 2);
    x_dist_ = grid.x.segment(1, x_size - 1) - grid.x.segment(0, x_size - 1);
    xu_dist_ = grid.xu.segment(2, x_size - 2) - grid.xu.segment(1, x_size - 2);

    right_wall_viscosity_coeff_ =
        config.viscosity / (grid.xu(x_size - 1) - grid.x(x_size - 2));
    left_wall_viscosity_coeff_ = config.viscosity / (grid.x(1) - grid.xu(1));

    update_equation(config, variables);

    solver_.init(1, x_size - 2, 2, y_size - 2, variables.v);
    residual_norm_before_update_ =
        solver_.residual_norm(equation_, variables.v);
}

void v_solver::solve(const simple_cavity2d_config& config, const grid& grid,
    fluid_variables& variables) {
    (void)grid;

    update_equation(config, variables);

    residual_norm_before_update_ =
        solver_.residual_norm(equation_, variables.v);

    update_variables(config, variables);

    residual_norm_ = solver_.residual_norm(equation_, variables.v);
}

auto v_solver::equation() const -> const linear_equation& { return equation_; }

auto v_solver::residual_norm() const -> double { return residual_norm_; }

auto v_solver::residual_norm_before_update() const -> double {
    return residual_norm_before_update_;
}

void v_solver::update_equation(
    const simple_cavity2d_config& config, const fluid_variables& variables) {
    const num_collect::index_type x_size = variables.v.rows();
    const num_collect::index_type y_size = variables.v.cols();

    // Convection term.
    const double half_density = 0.5 * config.density;
    cn_ = half_density *
        ((variables.v.middleCols(2, y_size - 3).middleRows(1, x_size - 2) +
             variables.v.middleCols(3, y_size - 3).middleRows(1, x_size - 2))
                .array()
                .colwise() *
            xu_dist_.array());
    cs_ = half_density *
        ((variables.v.middleCols(1, y_size - 3).middleRows(1, x_size - 2) +
             variables.v.middleCols(2, y_size - 3).middleRows(1, x_size - 2))
                .array()
                .colwise() *
            xu_dist_.array());
    ce_ = half_density *
        ((variables.u.middleCols(2, y_size - 3).middleRows(2, x_size - 2) +
             variables.u.middleCols(1, y_size - 3).middleRows(2, x_size - 2))
                .array()
                .rowwise() *
            y_dist_.transpose().array());
    cw_ = half_density *
        ((variables.u.middleCols(2, y_size - 3).middleRows(1, x_size - 2) +
             variables.u.middleCols(1, y_size - 3).middleRows(1, x_size - 2))
                .array()
                .rowwise() *
            y_dist_.transpose().array());

    // Diffusion term.
    dn_ = config.viscosity *
        (xu_dist_ * yv_dist_.segment(1, y_size - 3).cwiseInverse().transpose());
    ds_ = config.viscosity *
        (xu_dist_ * yv_dist_.segment(0, y_size - 3).cwiseInverse().transpose());
    de_ = config.viscosity *
        (x_dist_.segment(1, x_size - 2).cwiseInverse() * y_dist_.transpose());
    dw_ = config.viscosity *
        (x_dist_.segment(0, x_size - 2).cwiseInverse() * y_dist_.transpose());

    // Sum of above terms and pressure term.
    equation_.an.middleCols(2, y_size - 3).middleRows(1, x_size - 2) =
        (-cn_).cwiseMax(dn_ - 0.5 * cn_).cwiseMax(0.0);
    equation_.as.middleCols(2, y_size - 3).middleRows(1, x_size - 2) =
        cs_.cwiseMax(ds_ + 0.5 * cs_).cwiseMax(0.0);
    equation_.ae.middleCols(2, y_size - 3).middleRows(1, x_size - 2) =
        (-ce_).cwiseMax(de_ - 0.5 * ce_).cwiseMax(0.0);
    equation_.aw.middleCols(2, y_size - 3).middleRows(1, x_size - 2) =
        cw_.cwiseMax(dw_ + 0.5 * cw_).cwiseMax(0.0);
    equation_.ap.middleCols(2, y_size - 3).middleRows(1, x_size - 2) =
        equation_.an.middleCols(2, y_size - 3).middleRows(1, x_size - 2) +
        equation_.as.middleCols(2, y_size - 3).middleRows(1, x_size - 2) +
        equation_.ae.middleCols(2, y_size - 3).middleRows(1, x_size - 2) +
        equation_.aw.middleCols(2, y_size - 3).middleRows(1, x_size - 2) + cn_ -
        cs_ + ce_ - cw_;
    equation_.b.middleCols(2, y_size - 3).middleRows(1, x_size - 2) =
        (variables.p.middleCols(1, y_size - 3).middleRows(1, x_size - 2) -
            variables.p.middleCols(2, y_size - 3).middleRows(1, x_size - 2))
            .array()
            .colwise() *
        xu_dist_.array();

    // Boundary condition on the upper wall.
    equation_.an.col(y_size - 2).setZero();

    // Boundary condition on the lower wall.
    equation_.as.col(2).setZero();

    // Boundary condition on the right wall.
    equation_.ae.row(x_size - 2).setZero();
    equation_.ap.row(x_size - 2).segment(2, y_size - 3) +=
        right_wall_viscosity_coeff_ * y_dist_.transpose();

    // Boundary condition on the left wall.
    equation_.aw.row(1).setZero();
    equation_.ap.row(1).segment(2, y_size - 3) +=
        left_wall_viscosity_coeff_ * y_dist_.transpose();

    // Relaxation.
    equation_.ap.middleCols(2, y_size - 3).middleRows(1, x_size - 2) /=
        config.v_relax_coeff;
    equation_.b.middleCols(2, y_size - 3).middleRows(1, x_size - 2) +=
        (1.0 - config.v_relax_coeff) *
        equation_.ap.middleCols(2, y_size - 3)
            .middleRows(1, x_size - 2)
            .cwiseProduct(variables.v.middleCols(2, y_size - 3)
                              .middleRows(1, x_size - 2));

    pressure_rate_.middleCols(2, y_size - 3).middleRows(1, x_size - 2) =
        equation_.ap.middleCols(2, y_size - 3)
            .middleRows(1, x_size - 2)
            .cwiseInverse()
            .array()
            .colwise() *
        xu_dist_.array();
}

void v_solver::update_variables(
    const simple_cavity2d_config& config, fluid_variables& variables) {
    for (num_collect::index_type i = 0; i < config.v_tdma_iterations; ++i) {
        solver_.solve_for_y(equation_, variables.v);
    }
}

auto v_solver::pressure_rate() const -> const matrix_type& {
    return pressure_rate_;
}

}  // namespace fluid_sim::simple::cavity2d
