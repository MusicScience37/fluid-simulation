/*
 * Copyright 2021 MusicScience37 (Kenta Kabashima)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*!
 * \file
 * \brief Implementation of fluid_variables class.
 */
#include "fluid_sim/simple/cavity2d/fluid_variables.h"

#include "fluid_sim/simple/cavity2d/types.h"

namespace fluid_sim::simple::cavity2d {

auto make_fluid_variables(const grid& g) -> fluid_variables {
    return fluid_variables{.u = matrix_type::Zero(g.xu.size(), g.y.size()),
        .v = matrix_type::Zero(g.x.size(), g.yv.size()),
        .p = matrix_type::Zero(g.x.size(), g.y.size())};
}

}  // namespace fluid_sim::simple::cavity2d
