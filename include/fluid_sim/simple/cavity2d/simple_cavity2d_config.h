/*
 * Copyright 2021 MusicScience37 (Kenta Kabashima)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*!
 * \file
 * \brief Definition of simple_cavity2d_config class.
 */
#pragma once

#include <string>

#include <num_collect/base/index_type.h>

namespace fluid_sim::simple::cavity2d {

/*!
 * \brief Class of configurations of the solver.
 */
struct simple_cavity2d_config {
public:
    //! Density coefficient.
    double density{1e+3};  // NOLINT

    //! Viscosity coefficient.
    double viscosity{1e-3};  // NOLINT

    //! Width.
    double width{1.0};

    //! Height.
    double height{1.0};

    //! Velocity on the upper wall.
    double upper_wall_velocity{1e-4};  // NOLINT

    //! Number of points in the cavity for x-direction.
    num_collect::index_type x_points{10};  // NOLINT

    //! Number of points in the cavity for y-direction.
    num_collect::index_type y_points{10};  // NOLINT

    //! Number of iterations of TDMA for x-element of the velocity.
    num_collect::index_type u_tdma_iterations{5};  // NOLINT

    //! Number of iterations of TDMA for y-element of the velocity.
    num_collect::index_type v_tdma_iterations{5};  // NOLINT

    //! Number of iterations of TDMA for the pressure.
    num_collect::index_type p_tdma_iterations{10};  // NOLINT

    //! Relaxation coefficient for x-element of the velocity.
    double u_relax_coeff{0.3};  // NOLINT

    //! Relaxation coefficient for y-element of the velocity.
    double v_relax_coeff{0.3};  // NOLINT

    //! Relaxation coefficient for the pressure.
    double p_relax_coeff{0.5};  // NOLINT

    //! Maximum number of iterations in total solver.
    num_collect::index_type max_iterations{1000};  // NOLINT

    //! Rate of the tolerance of residuals.
    double residual_tolerance_rate{1e-4};  // NOLINT

    //! Use binary output.
    bool binary_output{true};

    //! Output filepath.
    std::string output_filepath{"simple_cavity.vts"};
};

/*!
 * \brief Load configuration.
 *
 * \param[in] filepath Filepath.
 * \return Configuration.
 */
[[nodiscard]] auto load_simple_cavity2d_config(const std::string& filepath)
    -> simple_cavity2d_config;

}  // namespace fluid_sim::simple::cavity2d
