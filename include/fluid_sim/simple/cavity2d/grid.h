/*
 * Copyright 2021 MusicScience37 (Kenta Kabashima)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*!
 * \file
 * \brief Definition of grid class.
 */
#pragma once

#include <num_collect/base/index_type.h>

#include "fluid_sim/simple/cavity2d/simple_cavity2d_config.h"
#include "fluid_sim/simple/cavity2d/types.h"

namespace fluid_sim::simple::cavity2d {

/*!
 * \brief Struct to save information of grids.
 *
 * \note Notation is as in \cite Arakawa2008.
 */
struct grid {
public:
    //! x for scalars.
    vector_type x{};

    //! x for velocities.
    vector_type xu{};

    //! y for scalars.
    vector_type y{};

    //! y for velocities.
    vector_type yv{};
};

/*!
 * \brief Create a grid.
 *
 * \param[in] config Configuration.
 * \return Grid.
 */
[[nodiscard]] auto make_grid(const simple_cavity2d_config&) -> grid;

}  // namespace fluid_sim::simple::cavity2d
