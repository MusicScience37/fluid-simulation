/*
 * Copyright 2021 MusicScience37 (Kenta Kabashima)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*!
 * \file
 * \brief Definition of tdma_linear_solver class.
 */
#pragma once

#include <limits>

#include <num_collect/base/index_type.h>

#include "fluid_sim/simple/cavity2d/types.h"

namespace fluid_sim::simple::cavity2d {

/*!
 * \brief Struct to save coefficients in linear equations.
 *
 * \note Notation is as in \cite Arakawa2008.
 */
struct linear_equation {
public:
    //! Coefficient of each position.
    matrix_type ap{};

    //! Coefficient of the north position.
    matrix_type an{};

    //! Coefficient of the south position.
    matrix_type as{};

    //! Coefficient of the west position.
    matrix_type aw{};

    //! Coefficient of the east position.
    matrix_type ae{};

    //! Remaining value.
    matrix_type b{};
};

/*!
 * \brief Class to solve linear equation using tri-diagonal matrix algorithm
 * (TDMA).
 *
 * \note Notation is as in \cite Arakawa2008.
 */
class tdma_linear_solver {
public:
    /*!
     * \brief Constructor.
     */
    tdma_linear_solver() = default;

    /*!
     * \brief Initialize.
     *
     * \param[in] x_min_ind Minimum index of x.
     * \param[in] x_max_ind Maximum index of x.
     * \param[in] y_min_ind Minimum index of y.
     * \param[in] y_max_ind Maximum index of y.
     * \param[in] mat Reference matrix to determine sizes.
     */
    void init(num_collect::index_type x_min_ind,
        num_collect::index_type x_max_ind, num_collect::index_type y_min_ind,
        num_collect::index_type y_max_ind, const matrix_type& mat);

    /*!
     * \brief Solve for y-direction.
     *
     * \param[in] equation Equation.
     * \param[in,out] x Solution.
     */
    void solve_for_y(const linear_equation& equation, matrix_type& x);

    /*!
     * \brief Calculate the norm of the residual.
     *
     * \param[in] equation Equation.
     * \param[in] x Solution.
     * \return Norm of the residual.
     */
    [[nodiscard]] auto residual_norm(
        const linear_equation& equation, const matrix_type& x) const -> double;

private:
    //! Minimum index of x.
    num_collect::index_type x_min_ind_{};

    //! Maximum index of x.
    num_collect::index_type x_max_ind_{};

    //! Minimum index of y.
    num_collect::index_type y_min_ind_{};

    //! Maximum index of y.
    num_collect::index_type y_max_ind_{};

    //! Intermediate coefficients.
    matrix_type p_{};

    //! Intermediate coefficients.
    matrix_type q_{};

    //! Intermidiate coefficients.
    vector_type den_{};
};

}  // namespace fluid_sim::simple::cavity2d
