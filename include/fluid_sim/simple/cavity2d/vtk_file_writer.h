/*
 * Copyright 2021 MusicScience37 (Kenta Kabashima)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*!
 * \file
 * \brief Definition of vtk_file_writer class.
 */
#pragma once

#include <memory>

#include <num_collect/logging/log_tag_view.h>
#include <num_collect/logging/logging_mixin.h>

#include "fluid_sim/simple/cavity2d/fluid_variables.h"
#include "fluid_sim/simple/cavity2d/grid.h"
#include "fluid_sim/simple/cavity2d/simple_cavity2d_config.h"

namespace fluid_sim::simple::cavity2d {

//! Log tag.
constexpr auto vtk_file_writer_tag = num_collect::logging::log_tag_view(
    "fluid_sim::simple::cavity2d::vtk_file_writer");

/*!
 * \brief Class to write files for VTK.
 */
class vtk_file_writer : public num_collect::logging::logging_mixin {
public:
    /*!
     * \brief Constructor.
     */
    vtk_file_writer();

    /*!
     * \brief Write a file.
     *
     * \param[in] config Configuration.
     * \param[in] grid Grid.
     * \param[in] variables Variables.
     * \param[in] filepath Filepath.
     */
    void write(const simple_cavity2d_config& config, const grid& grid,
        const fluid_variables& variables, const std::string& filepath);
};

}  // namespace fluid_sim::simple::cavity2d
