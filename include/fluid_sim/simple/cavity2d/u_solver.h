/*
 * Copyright 2021 MusicScience37 (Kenta Kabashima)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*!
 * \file
 * \brief Definition of u_solver class.
 */
#pragma once

#include <limits>

#include "fluid_sim/simple/cavity2d/fluid_variables.h"
#include "fluid_sim/simple/cavity2d/grid.h"
#include "fluid_sim/simple/cavity2d/simple_cavity2d_config.h"
#include "fluid_sim/simple/cavity2d/tdma_linear_solver.h"
#include "fluid_sim/simple/cavity2d/types.h"

namespace fluid_sim::simple::cavity2d {

/*!
 * \brief Class to solve the equation of the x-element of the velocity.
 */
class u_solver {
public:
    /*!
     * \brief Constructor.
     */
    u_solver() = default;

    /*!
     * \brief Initialize.
     *
     * \param[in] config Configuration.
     * \param[in] grid Grid.
     * \param[in] variables Variables.
     */
    void init(const simple_cavity2d_config& config, const grid& grid,
        const fluid_variables& variables);

    /*!
     * \brief Update the equation.
     *
     * \param[in] config Configuration.
     * \param[in] grid Grid.
     * \param[in,out] variables Variables.
     */
    void solve(const simple_cavity2d_config& config, const grid& grid,
        fluid_variables& variables);

    /*!
     * \brief Access the equation.
     *
     * \return Equation.
     */
    [[nodiscard]] auto equation() const -> const linear_equation&;

    /*!
     * \brief Get the norm of the last residual.
     *
     * \return Norm of the residual.
     */
    [[nodiscard]] auto residual_norm() const -> double;

    /*!
     * \brief Get the norm of the residual before the last update.
     *
     * \return Norm of the residual.
     */
    [[nodiscard]] auto residual_norm_before_update() const -> double;

    /*!
     * \brief Get the rate of the pressure in the solution.
     *
     * \return Rate.
     */
    [[nodiscard]] auto pressure_rate() const -> const matrix_type&;

private:
    /*!
     * \brief Update the equation.
     *
     * \note This will be called in solve function.
     *
     * \param[in] config Configuration.
     * \param[in] variables Variables.
     */
    void update_equation(
        const simple_cavity2d_config& config, const fluid_variables& variables);

    /*!
     * \brief Update the variables using the current equation.
     *
     * \note This will be called in solve function.
     *
     * \param[in] config Configuration.
     * \param[in,out] variables Variables.
     */
    void update_variables(
        const simple_cavity2d_config& config, fluid_variables& variables);

    //! Current linear equation.
    linear_equation equation_{};

    //! TDMA solver.
    tdma_linear_solver solver_{};

    //! Norm of the last residual.
    double residual_norm_{std::numeric_limits<double>::quiet_NaN()};

    //! Norm of the residual before the last update.
    double residual_norm_before_update_{
        std::numeric_limits<double>::quiet_NaN()};

    //! Rate of the pressure in the solution.
    matrix_type pressure_rate_{};

    /*!
     * \name Intermediate variables.
     */
    ///@{
    //! Intermediate variable.
    matrix_type cn_{};
    matrix_type cs_{};
    matrix_type ce_{};
    matrix_type cw_{};
    matrix_type dn_{};
    matrix_type ds_{};
    matrix_type de_{};
    matrix_type dw_{};
    vector_type x_dist_{};
    vector_type xu_dist_{};
    vector_type y_dist_{};
    vector_type yv_dist_{};
    double upper_wall_viscosity_coeff_{};
    double lower_wall_viscosity_coeff_{};
    ///@}
};

}  // namespace fluid_sim::simple::cavity2d
