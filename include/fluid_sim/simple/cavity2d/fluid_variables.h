/*
 * Copyright 2021 MusicScience37 (Kenta Kabashima)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*!
 * \file
 * \brief Definition of fluid_variables class.
 */
#pragma once

#include "fluid_sim/simple/cavity2d/grid.h"
#include "fluid_sim/simple/cavity2d/types.h"

namespace fluid_sim::simple::cavity2d {

/*!
 * \brief Struct of variables of fluid.
 */
struct fluid_variables {
public:
    //! x-element of the velocity.
    matrix_type u{};

    //! y-element of the velocity.
    matrix_type v{};

    //! Pressure.
    matrix_type p{};
};

/*!
 * \brief Prepare variables of fluid.
 *
 * \param[in] g Grid.
 * \return Variables.
 */
[[nodiscard]] auto make_fluid_variables(const grid& g) -> fluid_variables;

}  // namespace fluid_sim::simple::cavity2d
