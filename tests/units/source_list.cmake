set(SOURCE_FILES
    catch_event_listner.cpp
    main.cpp
    simple/cavity2d/grid_test.cpp
    simple/cavity2d/p_solver_test.cpp
    simple/cavity2d/tdma_linear_solver_test.cpp
    simple/cavity2d/u_solver_test.cpp
    simple/cavity2d/v_solver_test.cpp
    simple/cavity2d/vtk_file_writer_test.cpp
)
