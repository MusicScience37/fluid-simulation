/*
 * Copyright 2021 MusicScience37 (Kenta Kabashima)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*!
 * \file
 * \brief Definition of test_config function.
 */
#pragma once

#include "fluid_sim/simple/cavity2d/simple_cavity2d_config.h"

namespace fluid_sim_test::simple::cavity2d {

/*!
 * \brief Create configuration for test.
 *
 * \return Configuration.
 */
[[nodiscard]] inline auto test_config()
    -> fluid_sim::simple::cavity2d::simple_cavity2d_config {
    return fluid_sim::simple::cavity2d::simple_cavity2d_config{
        .width = 2.0,   // NOLINT
        .height = 3.0,  // NOLINT
        .x_points = 5,  // NOLINT
        .y_points = 6,  // NOLINT
        .binary_output = false};
};

}  // namespace fluid_sim_test::simple::cavity2d
