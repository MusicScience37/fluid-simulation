/*
 * Copyright 2021 MusicScience37 (Kenta Kabashima)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*!
 * \file
 * \brief Definition of float_scrubber function.
 */
#pragma once

#include <regex>
#include <string>

#include <ApprovalTests.hpp>
#include <fmt/format.h>

namespace fluid_sim_test::simple::cavity2d {

/*!
 * \brief Crete a scrubber function to replace floating point values.
 *
 * \return Scrubber.
 */
[[nodiscard]] inline auto float_scrubber() -> ApprovalTests::Scrubber {
    return ApprovalTests::Scrubbers::createRegexScrubber(
        std::regex(R"(-?\d\.\d+(e[+-]\d+)?)"),
        [](const std::ssub_match& match) -> std::string {
            if (match.str() == "1.0" || match.str() == "0.1") {
                return match.str();
            }
            const double val = std::stod(match.str());
            return fmt::format("{:.3e}", val);
        });
}

}  // namespace fluid_sim_test::simple::cavity2d
