/*
 * Copyright 2021 MusicScience37 (Kenta Kabashima)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*!
 * \file
 * \brief Test of tdma_linear_solver class.
 */
#include "fluid_sim/simple/cavity2d/tdma_linear_solver.h"

#include <ApprovalTests.hpp>
#include <catch2/catch_test_macros.hpp>
#include <fmt/format.h>
#include <num_collect/base/index_type.h>
#include <num_collect/util/format_dense_matrix.h>

#include "fluid_sim/simple/cavity2d/types.h"

TEST_CASE("fluid_sim::simple::cavity2d::tdma_linear_solver") {
    using fluid_sim::simple::cavity2d::linear_equation;
    using fluid_sim::simple::cavity2d::matrix_type;
    using fluid_sim::simple::cavity2d::tdma_linear_solver;

    SECTION("solve") {
        const num_collect::index_type size = 5;
        matrix_type true_x = matrix_type::Zero(size, size);
        for (num_collect::index_type j = 0; j < size; ++j) {
            for (num_collect::index_type i = 0; i < size; ++i) {
                true_x(i, j) = static_cast<double>(i * size + j);
            }
        }

        const matrix_type ap = matrix_type::Constant(size, size, 1.0);
        const matrix_type an = matrix_type::Constant(size, size, 0.1);
        const matrix_type as = matrix_type::Constant(size, size, 0.2);
        const matrix_type aw = matrix_type::Constant(size, size, 0.3);
        const matrix_type ae = matrix_type::Constant(size, size, 0.4);
        matrix_type b = matrix_type::Zero(size, size);
        for (num_collect::index_type j = 0; j < size; ++j) {
            for (num_collect::index_type i = 0; i < size; ++i) {
                b(i, j) = ap(i, j) * true_x(i, j);
                if (i > 0) {
                    b(i, j) -= aw(i, j) * true_x(i - 1, j);
                }
                if (i < size - 1) {
                    b(i, j) -= ae(i, j) * true_x(i + 1, j);
                }
                if (j > 0) {
                    b(i, j) -= as(i, j) * true_x(i, j - 1);
                }
                if (j < size - 1) {
                    b(i, j) -= an(i, j) * true_x(i, j + 1);
                }
            }
        }
        const linear_equation equation{
            .ap = ap, .an = an, .as = as, .aw = aw, .ae = ae, .b = b};

        matrix_type x = true_x;
        x.middleCols(1, size - 2).middleRows(1, size - 2).setZero();

        tdma_linear_solver solver;
        solver.init(1, size - 2, 1, size - 2, x);

        std::string x_change =
            fmt::format("{:.6f}\n", num_collect::util::format_dense_matrix(x));
        std::string res_change =
            fmt::format("{:.6f}\n", solver.residual_norm(equation, x));

        constexpr num_collect::index_type repetition = 10;
        for (num_collect::index_type i = 0; i < repetition; ++i) {
            solver.solve_for_y(equation, x);
            x_change += fmt::format(
                "{:.6f}\n", num_collect::util::format_dense_matrix(x));
            res_change +=
                fmt::format("{:.6f}\n", solver.residual_norm(equation, x));
        }

        ApprovalTests::Approvals::verify(
            fmt::format("Solution:\n"
                        "{}"
                        "Residual:\n"
                        "{}",
                x_change, res_change));
    }
}
