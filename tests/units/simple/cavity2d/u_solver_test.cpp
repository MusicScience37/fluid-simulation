/*
 * Copyright 2021 MusicScience37 (Kenta Kabashima)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*!
 * \file
 * \brief Test of u_solver class.
 */
#include "fluid_sim/simple/cavity2d/u_solver.h"

#include <ApprovalTests.hpp>
#include <catch2/catch_test_macros.hpp>
#include <fmt/format.h>
#include <num_collect/util/format_dense_matrix.h>

#include "float_scrubber.h"
#include "fluid_sim/simple/cavity2d/fluid_variables.h"
#include "fluid_sim/simple/cavity2d/grid.h"
#include "fluid_sim/simple/cavity2d/simple_cavity2d_config.h"
#include "fluid_sim/simple/cavity2d/vtk_file_writer.h"
#include "read_file.h"
#include "test_config.h"

TEST_CASE("fluid_sim::simple::cavity2d::u_solver") {
    using fluid_sim::simple::cavity2d::make_fluid_variables;
    using fluid_sim::simple::cavity2d::make_grid;
    using fluid_sim::simple::cavity2d::u_solver;
    using fluid_sim::simple::cavity2d::vtk_file_writer;
    using fluid_sim_test::simple::cavity2d::float_scrubber;
    using fluid_sim_test::simple::cavity2d::read_file;
    using fluid_sim_test::simple::cavity2d::test_config;
    using num_collect::util::format_dense_matrix;

    SECTION("init") {
        const auto config = test_config();
        const auto grid = make_grid(config);
        auto variables = make_fluid_variables(grid);

        u_solver solver;
        solver.init(config, grid, variables);

        ApprovalTests::Approvals::verify(
            fmt::format("residual_norm_before_update: {:.3e}",
                solver.residual_norm_before_update()));
    }

    SECTION("solve once") {
        const auto config = test_config();
        const auto grid = make_grid(config);
        auto variables = make_fluid_variables(grid);

        u_solver solver;
        solver.init(config, grid, variables);
        solver.solve(config, grid, variables);

        SECTION("residual") {
            ApprovalTests::Approvals::verify(
                fmt::format("residual_norm_before_update: {:.3e}\n"
                            "residual_norm_after_update:  {:.3e}\n"
                            "Solution:\n{:.3e}",
                    solver.residual_norm_before_update(),
                    solver.residual_norm(), format_dense_matrix(variables.u)));
        }

        SECTION("solution") {
            const std::string filepath{"simple_cavity_u_solver_test.vts"};
            vtk_file_writer writer;
            writer.write(config, grid, variables, filepath);

            ApprovalTests::Approvals::verify(read_file(filepath),
                ApprovalTests::Options()
                    .withScrubber(float_scrubber())
                    .fileOptions()
                    .withFileExtension(".vts"));
        }
    }
}
