/*
 * Copyright 2021 MusicScience37 (Kenta Kabashima)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*!
 * \file
 * \brief Test of grid class.
 */
#include "fluid_sim/simple/cavity2d/grid.h"

#include <ApprovalTests.hpp>
#include <catch2/catch_test_macros.hpp>
#include <fmt/format.h>
#include <num_collect/base/index_type.h>
#include <num_collect/util/format_dense_vector.h>

#include "fluid_sim/simple/cavity2d/simple_cavity2d_config.h"
#include "test_config.h"

TEST_CASE("fluid_sim::simple::cavity2d::make_grid") {
    using fluid_sim::simple::cavity2d::grid;
    using fluid_sim::simple::cavity2d::make_grid;
    using fluid_sim::simple::cavity2d::simple_cavity2d_config;
    using fluid_sim_test::simple::cavity2d::test_config;
    using num_collect::util::format_dense_vector;

    SECTION("make a grid") {
        const simple_cavity2d_config config = test_config();

        const grid g = make_grid(config);

        ApprovalTests::Approvals::verify(
            fmt::format("x:  {:.3f}\n"
                        "xu: {:.3f}\n"
                        "y:  {:.3f}\n"
                        "yv: {:.3f}",
                format_dense_vector(g.x), format_dense_vector(g.xu),
                format_dense_vector(g.y), format_dense_vector(g.yv)));
    }
}
