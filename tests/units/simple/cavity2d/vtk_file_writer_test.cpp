/*
 * Copyright 2021 MusicScience37 (Kenta Kabashima)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*!
 * \file
 * \brief Test of vtk_file_writer class.
 */
#include "fluid_sim/simple/cavity2d/vtk_file_writer.h"

#include <ApprovalTests.hpp>
#include <catch2/catch_test_macros.hpp>
#include <num_collect/base/index_type.h>

#include "float_scrubber.h"
#include "read_file.h"
#include "test_config.h"

TEST_CASE("fluid_sim::simple::cavity2d::vtk_file_writer") {
    using fluid_sim::simple::cavity2d::make_fluid_variables;
    using fluid_sim::simple::cavity2d::make_grid;
    using fluid_sim::simple::cavity2d::vtk_file_writer;
    using fluid_sim_test::simple::cavity2d::float_scrubber;
    using fluid_sim_test::simple::cavity2d::read_file;
    using fluid_sim_test::simple::cavity2d::test_config;

    SECTION("write a file") {
        auto config = test_config();
        const auto grid = make_grid(config);
        auto variables = make_fluid_variables(grid);

        const num_collect::index_type x_size = grid.x.size();
        const num_collect::index_type y_size = grid.y.size();

        for (num_collect::index_type j = 0; j < y_size; ++j) {
            for (num_collect::index_type i = 0; i < x_size; ++i) {
                variables.u(i, j) = static_cast<double>(i + j * x_size);
                variables.v(i, j) = static_cast<double>(i * y_size + j);
                variables.p(i, j) =
                    static_cast<double>(i - (j - y_size / 2) * 2);  // NOLINT
            }
        }

        vtk_file_writer writer;
        const std::string filepath{"simple_cavity_vtk_file_writer_test.vts"};
        writer.write(config, grid, variables, filepath);

        ApprovalTests::Approvals::verify(read_file(filepath),
            ApprovalTests::Options()
                .withScrubber(float_scrubber())
                .fileOptions()
                .withFileExtension(".vts"));

        config.binary_output = true;
        writer.write(config, grid, variables,
            "simple_cavity_vtk_file_writer_test_binary.vts");
    }
}
