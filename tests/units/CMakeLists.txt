include(${CMAKE_CURRENT_SOURCE_DIR}/source_list.cmake)
add_executable(fluid_sim_test_units ${SOURCE_FILES})
target_link_libraries(fluid_sim_test_units PRIVATE simple_cavity2d_lib)
target_add_catch2(fluid_sim_test_units)
target_add_ausan(fluid_sim_test_units)

add_executable(fluid_sim_test_units_unity EXCLUDE_FROM_ALL unity_source.cpp)
target_link_libraries(
    fluid_sim_test_units_unity
    PRIVATE Catch2::Catch2WithMain ApprovalTests::ApprovalTests
            simple_cavity2d_lib)
target_include_directories(fluid_sim_test_units_unity
                           PRIVATE ${FLUID_SIM_TEST_INCLUDE_DIR})
